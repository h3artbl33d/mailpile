# Welcome to Mailpile for OpenBSD! #

Mailpile (<https://www.mailpile.is/>) is a modern, fast web-mail client
with user-friendly encryption and privacy features. The development of
Mailpile is funded by
[a large community of backers](https://www.mailpile.is/#community)
and all code related to the project is and will be released under an OSI
approved Free Software license.

Mailpile places great emphasis on providing a clean, elegant user
interface and pleasant user experience. In particular, Mailpile aims to
make it easy and convenient to receive and send PGP encrypted or signed
e-mail.

Mailpile's primary user interface is web-based, but it also has a basic
command-line interface and an API for developers. Using web technology
for the interface allows Mailpile to function both as a local desktop
application (accessed by visiting `localhost` in the browser) or a
remote web-mail on a personal server or VPS.

The core of Mailpile is a fast search engine, custom written to deal
with large volumes of e-mail on consumer hardware. The search engine
allows e-mail to be organized using tags (similar to GMail's labels) and
the application can be configured to automatically tag incoming mail
either based on static rules or bayesian classifiers.

### IMPORTANT

This fork is solely for OpenBSD. Support for other operating systems
has been or will be stripped out completely. Please, do not ask to
support any other operating system - follow the upstream project if
you desire support for Linux, Windows, MacOS etc.
