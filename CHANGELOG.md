# Mailpile changelog

### Version 20190701
* Switched to keys.openpgp.org as per https://gist.github.com/rjhansen/67ab921ffb4084c865b3618d6955275f

### Version 20190610
* Forked from [Mailpile #2e6525f](https://github.com/mailpile/Mailpile/commit/2e6525f79f90870a6790e954f6df74f0c446ee03)
* Merged [PR#2205](https://github.com/mailpile/Mailpile/pull/2205) for vCal support
